﻿using System.Collections;
using UnityEngine;

public class RocketTower : MonoBehaviour
{
    [SerializeField] Transform spawner = null;
    [SerializeField] GameObject rocket = null;
    
    void Start()
    {
        StartCoroutine(RocketSpawner());
    }

    IEnumerator RocketSpawner()
    {
        yield return new WaitForSeconds(3f);

        Instantiate(rocket, spawner.position, Quaternion.identity);

        StartCoroutine(RocketSpawner());
    }
}
