﻿using UnityEngine;
using TMPro;

public class Health : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI healthText = null;

    int maxHealthPoints = 5;
    int currentHealthPoints;

    void Start()
    {
        currentHealthPoints = maxHealthPoints;
        UpdateHealthText();
    }

    public void Decrease()
    {
        currentHealthPoints--;
        UpdateHealthText();
    }

    void UpdateHealthText()
    {
        healthText.text = "Health : " + currentHealthPoints + " / " + maxHealthPoints;
    }
}
