﻿using UnityEngine;

public class Rocket : MonoBehaviour
{
    GameObject player;
    [SerializeField] float speed = 10f;

    Vector3 target;
    Vector3 currentVelocity;
    float rotationSpeed = 1f;

    bool hasCollided = false;
    bool isHoming = true;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        target = player.transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.up);
    }

    void Update()
    {
        Vector3 homingDirection = player.transform.position - transform.position;

        if (isHoming)
        {
            SeekOutPlayer(homingDirection);
        }
        
    }

    private void SeekOutPlayer(Vector3 targetDirection)
    {
        target = Vector3.RotateTowards(transform.forward, targetDirection, rotationSpeed * Time.deltaTime, 0f);
        transform.rotation = Quaternion.LookRotation(target);
        transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (hasCollided) return;

        hasCollided = true;

        if(other.collider.CompareTag("Player"))
        {
            FindObjectOfType<Health>().Decrease();
        }

        Destroy(gameObject);
    }
}
