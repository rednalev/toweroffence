﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    CharacterController characterController;
    Camera mainCamera;

    float speed = 5f;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        mainCamera = Camera.main;
    }

    private void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        float forwardMovement = horizontal * speed * Time.deltaTime;
        float rightMovement = vertical * speed * Time.deltaTime;

        Vector3 motion = SetMovementSpaceToCamera(forwardMovement, rightMovement);

        characterController.Move(motion);
    }

    private Vector3 SetMovementSpaceToCamera(float forwardMovement, float rightMovement)
    {
        Vector3 newMotion = Vector3.zero;

        newMotion += mainCamera.transform.forward * rightMovement;
        newMotion += mainCamera.transform.right * forwardMovement;
        newMotion.y = 0f;

        return newMotion;
    }
}
